## Docker environment for web development:

# Docker Compose

## docker & docker-compose
* [nginx:stable](https://hub.docker.com/_/nginx)
* [php:7.3.11-fpm](https://hub.docker.com/_/php)
* [php:7.3.11-apache-buster](https://hub.docker.com/_/php)
* [mariadb:10.3.20](https://hub.docker.com/_/mariadb)
* [phpmyadmin:latest](https://hub.docker.com/r/phpmyadmin/phpmyadmin)
* [adminer:stable](https://hub.docker.com/_/adminer)

    cp env-example .env

Edit the file .env

Add the domain

    127.0.0.1 localhost

On the browser:

    http://localhost
    http://localhost:81

### Start docker-compose

    docker-compose up -d nginx php-fpm mariadb adminer
    docker-compose up -d php_apache mariadb phpmyadmin

### Stop docker-compose

    docker-compose down

### Restart docker-compose

    docker-compose restart

# Version Control System
![git](https://git-scm.com/images/logo.png) 

* [GIT](https://git-scm.com/)

[Sérgio Moreira](https://samoreira.eu)
